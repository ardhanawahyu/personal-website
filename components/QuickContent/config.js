// import {
//   ArrowLeftRight,
//   ArrowRightLeft,
//   Code,
//   TextCursorInput
// } from 'lucide-react'

export const links = [
  {
    // title: 'Reach me out here',
    children: [
      {
        // icon: <ArrowLeftRight width={16} />, example of use
        title: 'LinkedIn',
        description: 'Connect with me on LinkedIn',
        href: 'https://www.linkedin.com/in/ardhanaawahyu/'
      },
      {
        title: 'Dealls!',
        description: 'Book a session mentoring 1-on-1 with me on Dealls!',
        href: 'https://dealls.com/mentoring/muhammad-ardhana-wahyu-nugraha-744/'
      },
      {
        title: 'GitHub',
        description: 'Explore my projects on GitHub',
        href: 'https://github.com/mardhanawn/'
      },
      {
        title: 'GitLab',
        description: 'Explore my projects on GitLab',
        href: 'https://gitlab.com/ardhanawahyu'
      },
      {
        title: 'Showwcase',
        description: 'Explore my profile on Showwcase',
        href: 'https://www.showwcase.com/ardhanaawahyu'
      }
    ]
  }
]

export const recommendations = [
  {
    title: 'Books',
    children: [
      {
        title: 'Atomic Habit',
        description:
          'Written by James Clear, this book is about the science of habit formation and how small changes can lead to remarkable results. Provides practical strategies to build good habits and break bad ones, making it easier to achieve your goals.',
        href: ''
      },
      {
        title: 'Think and Grow Rich',
        description:
          'Written by Napoleon Hill, this book is about how to become rich by thinking.',
        href: ''
      },
      {
        title: 'Exponential Living',
        description:
          'Written by Sheri Riley, this book is about living a balanced life where you can pursue your career aspirations without sacrificing your personal well-being. It provides a roadmap to stop spending 100% of your time on 10% of who you are.',
        href: ''
      },
      {
        title: 'The Psychology of Money',
        description:
          'Written by Morgan Housel, this book is about understanding the nuances of money and how our behavior influences our financial decisions. It provides insights on how to manage money better and make smarter financial choices.',
        href: ''
      },
      {
        title: 'Fear of Missing Out',
        description:
          'Written by Patrick J. McGinnis, this book is about how to overcome the fear of missing out and make better decisions in life. It provides strategies to focus on what truly matters and avoid distractions that lead to regret.',
        href: ''
      }
    ]
  },
  {
    title: 'Podcasts',
    children: [
      {
        title: 'TEDx Talks',
        description: '',
        href: 'https://www.youtube.com/@TEDx'
      },
      {
        title: 'Thirty days of Lunch',
        description: '',
        href: 'https://www.youtube.com/@thirtydaysoflunch'
      },
      {
        title: 'Kumpul Leaders',
        description: '',
        href: 'https://www.youtube.com/playlist?list=PLrOPOjXQhbEbG6oUOLerS7HGfry8VCRMc'
      },
      {
        title: 'In our Twenties',
        description: '',
        href: 'https://www.youtube.com/@inourtwentiesid'
      }
    ]
  }
]

export const topics = [
  {
    children: [
      {
        id: 1,
        title: 'Career Planning',
        description:
          'Explore career paths, identify abilities, & plan to achieve career goals.'
      },
      {
        id: 2,
        title: 'Industry Insights',
        description:
          'Manage workload, set boundaries & maintain a healthy work-life balance.'
      },
      {
        id: 3,
        title: 'Networking & Relationship',
        description:
          'Build professional networks, fostering peers or professional relationships.'
      },
      {
        id: 4,
        title: 'Personal Development',
        description: 'Develop skills & habits that promote personal growth.'
      },
      {
        id: 5,
        title: 'Leadership & Management',
        description:
          'Develop leadership & managerial skills, managing teams, delegating tasks.'
      }
    ]
  }
]

export const testimonials = [
  {
    children: [
      {
        id: 1,
        title: 'Zalva Ihilani P, Telkom University',
        description:
          'Bang Ardhan merupakan salah satu mentor yang berpengaruh dalam proses pembelajaran frontend saya. Dia memiliki pengetahuan yang luas di bidang ini dan selalu siap membantu dengan solusi yang tepat dan efisien, setiap sesi mentorship dengan Bang Ardhan terasa sangat produktif. Bang Ardhan mampu menjelaskan konsep-konsep yang rumit dengan cara yang mudah dipahami, bahkan untuk topik yang sebelumnya terasa sulit. Dia juga sangat sabar dan mendukung, selalu memberikan motivasi ketika saya merasa stuck. Saya sangat berterima kasih, karena sekarang proses belajar saya menjadi lebih terarah.',
        href: 'https://id.linkedin.com/in/zalva-ihilani-pasha-b88506237'
      },
      {
        id: 2,
        title: 'Adip Idi Surya, Universitas Negeri Jakarta',
        description:
          'Ketika saya lagi butuh mentor terkait proses awal menjadi junior developer. Mas Ardhan adalah mentor terbaik. Mas Ardhan bisa memberikan ilmunya kepada saya dalam mengarahkan jalan untuk terjun ke dunia Frontend Developer dari segala aspek.',
        href: 'https://id.linkedin.com/in/adip-idi-surya-772630224'
      },
      {
        id: 3,
        title: 'Raflyan Akhyar, AMIKOM Purwokerto',
        description:
          'Pengalaman saat mentor dengan Ardhan sangat berharga buat saya. Ardhan sangat mempengaruhi saya dalam dunia Frontend. Ketika saya bingung tentang sesuatu yang berhubungan dengan Frontend, Ardhan dapat menjelaskan dengan jelas kepada saya tentang penyelesaian tersebut. Saat mentoring saya meminta untuk mereview coding frontend saya, setelah melihat review saya sangat dengan review arda karena review tersebut dilakukan dengan sangat detail apa yang kurang tepat untuk coding saya.',
        href: 'https://id.linkedin.com/in/raflyanakhyar'
      },
      {
        id: 4,
        title: 'Doni Wahyudi, Politeknik Negeri Jember',
        description:
          'Mas Wahyu frontend yang saya pilih sebagai mentor yang bisa mengajarkan tentang apa itu frontend dan seperti apa roadmap frontend, menjelaskan semua pertanyaan, memberikan saran-saran yang berkaitan.',
        href: 'https://id.linkedin.com/in/doni-wahyudi-2b45b3260'
      }
    ]
  }
]

export const galleries = [
  {
    children: [
      {
        id: 1,
        src: '/gallery-2.png',
        alt: 'gallery-1'
      },
      {
        id: 1,
        src: '/gallery-1.png',
        alt: 'gallery-1'
      }
    ]
  }
]
