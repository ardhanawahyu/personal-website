'use client'
import { ChevronRight } from 'lucide-react'
import Link from 'next/link'
import { LayoutGroup, motion } from 'framer-motion'
import { usePathname } from 'next/navigation'
import { useState } from 'react'
import styles from './QuickContent.module.css'

export default function QuickContent({ exclude = '', content, isLink }) {
  const [activeIndex, setActiveIndex] = useState(0)
  const pathname = usePathname()

  return (
    <div className={styles.container}>
      <nav className={styles.navigation}>
        <LayoutGroup id={`layout-${pathname}`}>
          {content.map(({ title }, index) => {
            return (
              <motion.button
                key={`button-${title}`}
                className={styles.navigationItem}
                onClick={() => setActiveIndex(index)}
                data-active={index === activeIndex}
              >
                {index === activeIndex && (
                  <motion.div
                    className={styles.indicator}
                    layoutId="active-indicator"
                  />
                )}
                {title}
              </motion.button>
            )
          })}
        </LayoutGroup>
      </nav>
      {isLink && <p>Get to know me better through these platform</p>}
      {content
        .filter(({ title }) => title === content[activeIndex].title)
        .map(({ children, title }) => {
          return (
            <div className={styles.links} key={title}>
              {children
                .filter(({ title }) => title !== exclude)
                .map(({ title, description, href }) => {
                  return (
                    <Link
                      href={href}
                      key={title}
                      target="blank"
                      rel="nooper noreferrer"
                      className={styles.link}
                    >
                      <div className={styles.heading}>
                        {title}
                        <span className={styles.chevron}>
                          <ChevronRight width={16} />
                        </span>
                      </div>
                      <p className={styles.description}>{description}</p>
                    </Link>
                  )
                })}
            </div>
          )
        })}
    </div>
  )
}
