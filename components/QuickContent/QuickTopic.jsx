'use client'
import styles from './QuickContent.module.css'

export default function QuickTopic({ exclude = '', content }) {
  return (
    <div className={styles.container}>
      <h5>Topic mentoring</h5>
      {content.map(({ children }, key) => {
        return (
          <div className={styles.links} key={key}>
            {children.map(({ id, title, description }) => {
              return (
                <div className={styles.link} key={id}>
                  <div className={styles.heading}>
                    {title}
                    <span className={styles.chevron}></span>
                  </div>
                  <p className={styles.description}>{description}</p>
                </div>
              )
            })}
          </div>
        )
      })}
    </div>
  )
}
