'use client'
import Link from 'next/link'
import { ChevronRight } from 'lucide-react'
import styles from './QuickContent.module.css'

export default function QuickTestimonial({ exclude = '', content }) {
  return (
    <div className={styles.container}>
      <h5>What they say</h5>
      {content.map(({ children }, key) => {
        return (
          <div className={styles.links_full} key={key}>
            {children.map(({ id, title, description, href }) => {
              return (
                <Link
                  href={href}
                  key={id}
                  target="blank"
                  rel="nooper noreferrer"
                  className={styles.link}
                  data-umami-event={title}
                >
                  <div className={styles.heading}>
                    {title}
                    <span className={styles.chevron}>
                      <ChevronRight width={16} />
                    </span>
                  </div>
                  <p className={styles.description}>{description}</p>
                </Link>
              )
            })}
          </div>
        )
      })}
    </div>
  )
}
