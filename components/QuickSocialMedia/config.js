// import {
//   ArrowLeftRight,
//   ArrowRightLeft,
//   Code,
//   TextCursorInput
// } from 'lucide-react'

export const links = [
  {
    title: 'Reach me out here',
    children: [
      {
        // icon: <ArrowLeftRight width={16} />, example of use
        title: 'LinkedIn',
        description: 'Connect with me on LinkedIn',
        href: 'https://www.linkedin.com/in/ardhanaawahyu/'
      },
      {
        title: 'Dealls!',
        description: 'Book a session mentoring 1-on-1 with me on Dealls!',
        href: 'https://dealls.com/mentoring/muhammad-ardhana-wahyu-nugraha-744/'
      },
      {
        title: 'GitHub',
        description: 'Explore my projects on GitHub',
        href: 'https://github.com/mardhanawn/'
      },
      {
        title: 'GitLab',
        description: 'Explore my projects on GitLab',
        href: 'https://gitlab.com/ardhanawahyu'
      },
      {
        title: 'Showwcase',
        description: 'Explore my profile on Showwcase',
        href: 'https://www.showwcase.com/ardhanaawahyu'
      }
    ]
  }
]
