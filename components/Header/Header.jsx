import { ArrowUpRight } from 'lucide-react'
import Link from 'next/link'

import styles from './Header.module.css'

export default function Header({ children, links }) {
  return (
    <div className={styles.container}>
      <h5>{children}</h5>
      {links && (
        <div className={styles.links}>
          {links.map(({ title, href }) => {
            return (
              <Link
                href={href}
                key={href}
                target="_blank"
                data-umami-event={title}
              >
                <span className={styles.link}>
                  {title} <ArrowUpRight width={14} strokeWidth={2.5} />
                </span>
              </Link>
            )
          })}
        </div>
      )}
    </div>
  )
}
