import QuickContent from '@/components/QuickContent/QuickContent'
import styles from './page.module.css'
// import Image from 'next/image'
import { testimonials, topics } from '@/components/QuickContent/config'
import Header from '@/components/Header/Header'
import QuickTopic from '@/components/QuickContent/QuickTopic'
import QuickTestimonial from '@/components/QuickContent/QuickTestimonial'

export default function Home() {
  return (
    <main className={styles.main}>
      <div>
        <div>
          <p>Welcome Peeps! Here I am</p>
        </div>
        <h1>Ardhana Wahyu</h1>
        <h2>Software Engineer - Frontend and Mentor</h2>
        {/* <Image
          src="/ardhana.png"
          width="500"
          height="444"
          alt="Ardhana"
          className={styles.image}
        /> */}
        <p>
          Based in South Jakarta, Indonesia. I’m a Frontend Developer with over
          four years diving deep into React ecosystem. Currently, I'm helping
          companies in the Oil and Gas industry to develop Quantity Assurance
          products to unlock economic potential and ensure accountability.
        </p>

        <p>
          I am passionate about mentoring, I've successfully guided 20+ mentees
          from Codemasters.id, Bearmentor, and Dealls Mentoring Platform within
          less than a year. I offer 1-on-1 mentorship sessions tailored to
          individual goals and challenges.
        </p>

        <Header
          links={[
            {
              title: 'Dealls: Jobs & Mentoring',
              href: 'https://dealls.com/mentoring/muhammad-ardhana-wahyu-nugraha-744'
            },
            {
              title: 'Bearmentor',
              href: 'https://bearmentor.com/'
            }
          ]}
        >
          Interested in mentoring? Book a session here!
        </Header>

        <Header
          links={[
            {
              title: 'Moments in session',
              href: 'https://drive.google.com/drive/folders/1-4mqHHKdr8i5rNB7kATBdN1yq4ndOlSW?usp=sharing'
            }
          ]}
        >
          Snapshot of mentoring
        </Header>

        <p>
          My mentees come from diverse educational and professional backgrounds,
          which makes every discussion unique and insightful. If you're
          interested, here are some topics that might match what you're looking
          for.
        </p>
      </div>
      {/* <QuickContent content={links} isLink /> */}
      <QuickTopic content={topics} />
      <QuickTestimonial content={testimonials} />
    </main>
  )
}
