import './globals.css'
import { Inter, Roboto_Mono } from 'next/font/google'
import Script from 'next/script'
import { Analytics } from '@vercel/analytics/next'
import { SpeedInsights } from '@vercel/speed-insights/next'
import Navigation from '@/components/Navigation/Navigation'

const inter = Inter({ subsets: ['latin'], variable: '--font-inter' })
const robotoMono = Roboto_Mono({
  subsets: ['latin'],
  variable: '--font-roboto-mono'
})

export const metadata = {
  title: 'Ardhana Wahyu - Software Engineer',
  description: 'Get up to speed with Evervault',
  openGraph: {
    title: 'Ardhana Wahyu - Software Engineer',
    description: 'Personal Website v0',
    // url: 'https://evervault-starter-kit.vercel.app',
    siteName: 'Ardhana Wahyu - Software Engineer',
    // images: [
    //   {
    //     url: '/og-image.jpg',
    //     width: 1200,
    //     height: 630
    //   }
    // ],
    locale: 'en_US',
    type: 'website'
  },
  twitter: {
    card: 'summary_large_image',
    title: 'Ardhana Wahyu - Software Engineer',
    description: 'Get up to speed with Evervault'
    // images: ['/og-image.jpg']
  }
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body
        className={`${inter.variable} font-sans; ${robotoMono.variable} font-monospace;`}
      >
        <Script
          src="https://cloud.umami.is/script.js"
          data-website-id="9e6bd711-d880-4945-914f-554c37493b41"
        />
        <Navigation />
        <div className="rootLayout">{children}</div>
        <Analytics />
        <SpeedInsights />
      </body>
    </html>
  )
}
